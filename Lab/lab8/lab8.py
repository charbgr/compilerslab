import plex

#for example #3
letter  = plex.Range('AZaz')
digit   = plex.Range('09')
name    = letter + plex.Rep(letter|digit)
space   = plex.Rep1(plex.Any(' \t\n'))
keyword = plex.Str('if', 'then', 'else', 'end')
#end of example

lexicon = plex.Lexicon(
#	"""#1
#	[
#		(plex.Str('abc'), "ABC_TOKEN"),
#		(plex.Str('123'), "123_TOKEN"),
#		(plex.Rep1(plex.Any(' \t\n')), plex.IGNORE)
#	]
#	
#	
#	[
#		(plex.Str('tom'), 'TOM_TOKEN'),
#		(plex.Str('cat'), 'CAT_TOKEN'),
#		(plex.Str('tomcat'), 'TOMCAT_TOKEN'),
#		(plex.Rep1(plex.Any(' \t\n')), plex.IGNORE)
#	]
#	"""
	[
		
		(keyword, plex.TEXT),
		(name, 'IDENTIFIED'),
		(space, plex.IGNORE)
	]
)


#fp = open('plex1.txt', 'r') #1
#fp = open('plex2.txt', 'r') #2
fp = open('plex3.txt', 'r')
scanner = plex.Scanner(lexicon, fp)
while True:
	try:
		token, text = scanner.read()
	except plex.errors.PlexError:
		fileName, row, character = scanner.position()
		#character 0-base
		#filename  1-base
		print "Check your code at row", row, ".Character Position:", character+1
		break
	if token is None:
		break
	print token, text

fp.close()
