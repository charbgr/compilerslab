#!/usr/bin/python
# -*- coding: utf_8 -*-

"""
A blank (no function) MyParser class, to be used for developing recursive descent parsers.
Uses plex as scanner module.
"""


import plex


class ParseError(Exception):
	""" A user defined exception class, to describe parse errors. """
	pass	# we don't need to override anything here!


class RunError(Exception):
	""" A user defined exception class, to describe runtime errors. """
	pass	# we don't need to override anything here!


class MyParser:
	""" A class encapsulating all parsing (and executing) functionality
	for a particular grammar. """
	
	def __init__(self):
		self.st = {}

	
	def match(self, token):
		if self.la == token:
			self.la, self.val = self.next_token()
		else:
			raise ParseError('Found: ' + self.la + '\nExpected: ' + token)

	def addop(self):
		if self.la in ('+','-'):
			v = self.val
			self.match(self.la)
			return v
		else:
			raise ParseError('Expected: +,- got: ' + self.la)

	def multop(self):
		if self.la in ('*','/'):
			v = self.val
			self.match(self.la)
			return v
		else:
			raise ParseError('Expected: *,/ got: ' + self.la)

	def factor(self):
		if self.la == 'FLOAT':
			v = float(self.val)
			self.match(self.la)
			return v
		elif self.la == 'IDF':
			var = self.val
			self.match(self.la)

			if var in self.st:
				return self.st[var]
			else:
				raise RunError('Variable ' + var + ' does not have a value')
		elif self.la == '(':
			self.match('(')
			e = self.expr()
			self.match(')')

			return e
		else:
			raise ParseError('Expected: (,IDF,FLOAT got: ' + self.la)

	def factor_tail(self):
		if self.la in ('*','/'):
			rop = self.multop()
			rf  = self.factor()
			ft  = self.factor_tail()
			#if ft is not None:
			#	if ft[0] == '*':
			#		rf *= ft[1]
			#	else:
			#		rf /= ft[1]
				
				

			return (rop, rf, ft)
			
		elif self.la in ('+','-','IDF', 'print', 'EOT', ')'):
			return
		else:
			raise ParseError('Expected: *,/ got: ' + self.la)

	def stmt(self):
		if(self.la == 'IDF'):
			var = self.val
			self.match('IDF')
			self.match('=')
			self.st[var] = self.expr()
		elif(self.la == 'print'):
			self.match('print')
			print self.expr()
		else:
			raise ParseError('Expected: IDF,print got: ' + self.la)

	def stmt_list(self):
		if(self.la in ('IDF','print')):
			self.stmt()
			self.stmt_list()
		elif(self.la == 'EOT'):
			return
		else:
			raise ParseError('Expected: IDF,print got: ' + self.la)

	def expr(self):
		if(self.la in ('(','IDF','FLOAT')):
			rf = self.term()
			tt = self.term_tail()
			while tt is not None:
				if tt[0] == '+':
					rf += tt[1]
				else:
					rf -= tt[1]
				tt = tt[2]

			return rf
		else:
			raise ParseError('Expected: (,IDF,FLOAT got: ' + self.la)

	def term(self):
		if(self.la in ('(','IDF','FLOAT')):
			rf = self.factor()
			ft = self.factor_tail()
			while ft is not None:
				if ft[0] == '*':
					rf *= ft[1]
				else:
					rf /= ft[1]
				ft = ft[2]

			return rf
		else:
			raise ParseError('Expected: (,IDF,FLOAT got: ' + self.la)

	def term_tail(self):
		if(self.la in ('+','-')):
			rop = self.addop()
			rf  = self.term()
			tt  = self.term_tail()
			#if tt is not None:
			#	if tt[0] == '+':
			#		rf += tt[1]
			#	else:
			#		rf -= tt[1]

			return (rop, rf, tt)
		elif(self.la in ('IDF', 'print', 'EOT', ')')):
			return
		else:
			raise ParseError('Expected: +,- got: ' + self.la)



	def create_scanner(self,fp):
		""" Creates a plex scanner for a particular grammar 
		to operate on file object fp. """

		operator = plex.Any('+-*/=()')
		keyword  = plex.Str('print')
		comment  = plex.Str('{') + plex.Rep(plex.AnyBut('}')) + plex.Str("}")
		single_string = plex.Str("'") + plex.Rep(plex.AnyBut("'")) + plex.Str("'")
		double_string = plex.Str('"') + plex.Rep(plex.AnyBut('"')) + plex.Str('"')
		letter = plex.Range('AZaz')
		digit  = plex.Range('09')
		hex1   = plex.Str('0x') + plex.Rep1(plex.Range('09afAF'))
		float1 = plex.Rep(digit) + plex.Str('.') + plex.Rep1(digit)
		name   = letter + plex.Rep(letter | digit)


		space  = plex.Rep1(plex.Any(' \t\n'))

		lexicon = plex.Lexicon([
		    (keyword  , plex.TEXT),
		    (operator , plex.TEXT),
		    (float1   , 'FLOAT'),
		    (name     , 'IDF'),
		    (space | comment , plex.IGNORE)
		])
		
		self.scanner = plex.Scanner(lexicon,fp)	# create and store the scanner object
		self.la, self.val = self.next_token()


	def next_token(self):
		""" Returns tuple (next_token,matched-text), 
		    if end-of-text returns ('EOT','').
		    Scanner errors are expected to be handled in outer code. """
		
		token,matched = self.scanner.read()		
		if token==None: return ('EOT','')
		return (token,matched)
	
	
	def parse_file(self,fp):
		""" Creates scanner for input file object fp and calls the parse logic code. """

		# create the plex scanner for fp
		self.create_scanner(fp)
		# call parsing logic
		self.stmt_list()

	
	def get_position(self):
		""" Utility function that returns position in text in case of errors.
		Here it simply returns the scanner position. """
		
		return self.scanner.position()
	


# the main part of prog

# create the parser object
parser = MyParser()

# open file for parsing
fp = open("some-parsing.txt","r")

# parse file and execute instructions
try:
	parser.parse_file(fp)
except plex.errors.PlexError:
	fname,lineno,charno = parser.get_position()	
	print "Scanner Error: at line %s char %s" % (lineno,charno+1)
except ParseError as perr:
	fname,lineno,charno = parser.get_position()	
	print "Parser Error: %s at line %s" % (perr,lineno)
except RunError as rerr:
	fname,lineno,charno = parser.get_position()	
	print "Runtime Error: %s at line %s" % (rerr,lineno)
finally:
	fp.close()	# close file, no matter what happens in parse_file()
