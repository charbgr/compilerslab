import plex

class ParseError(Exception):
    pass

class MyParser:
	symbol_table = None
	def __init__(self):
		self.symbol_table = {}

	def create_scanner(self, fp):
		letter = plex.Range("AZaz")
		digit = plex.Range("09")
		name = letter + plex.Rep(letter|digit)
		telesths = plex.Any("+-=")
		floats = plex.Rep(digit) + plex.Str(".") + plex.Rep1(digit)
		space = plex.Any(" \t\n")
		keyword = plex.Str("print")

		lexicon = plex.Lexicon([
			(floats, "FLOATCONST"),
			(keyword, "COMMAND"),
			(telesths, "OPERATOR"),
			(name, "IDENTIFIER"),
			(space, plex.IGNORE)
		])

		self.scanner = plex.Scanner(lexicon, fp)


	def parse_file(self, fp):
		self.create_scanner(fp)
		self.parse()

	def parse(self):
		curr_var = None
		while True:
			token, text = self.scanner.read()
			f, l, c = self.scanner.position()
			if token is None:
				break
			
			if token == "IDENTIFIER":
				curr_var = text
				token, text = self.scanner.read()
				if token == "OPERATOR" and text == "=":

					first_var_token, first_var = self.scanner.read()
					if first_var_token == "FLOATCONST":
						first_var = float(first_var)
					elif first_var_token == "IDENTIFIER":
						if first_var not in self.symbol_table:
							raise ParseError("You didn't define ", first_var)
						first_var = self.symbol_table[first_var]
					else:
						raise ParseError("waiting for a value or identifier")

					token, op = self.scanner.read()
					if token == "OPERATOR":
						sec_var_token, sec_var = self.scanner.read()
						if sec_var_token == "FLOATCONST":
							sec_var = float(sec_var)
						elif sec_var_token == "IDENTIFIER":
							if sec_var not in self.symbol_table:
								raise ParseError("You didn't define ", sec_var)
							sec_var = self.symbol_table[sec_var]
						else:
							raise ParseError("waiting for a value or an identifier")

						if op == "+":
							self.symbol_table[curr_var] = first_var + sec_var
						elif op == "-":
							self.symbol_table[curr_var] = first_var - sec_var
						else:
							raise ParseError("unknown operator")
							
					else:
						raise ParseError('unknown token')
				else:
				    raise ParseError("Unknown operator", text)
			elif token == "COMMAND":
				if text == "print":
					token, var = self.scanner.read()
					if token == "IDENTIFIER":
						print self.symbol_table[var]
			else:
				raise ParseError('waiting for command or an identifier...')

	def get_position(self):
		return self.scanner.position()


if __name__ == "__main__":
	parser = MyParser()	
	fp = open("test.txt", "r")
	try:
		parser.parse_file(fp)
	except plex.errors.PlexError:
		f, l, c = parser.get_position()
		print "ERROR IN LINE: ", l, " CHAR: ", c+1
	except ParseError as e:
	    f, l, c = parser.get_position()
	    print e
	    print "ERROR IN LINE: ", l, " CHAR: ", c+1
	finally:
		fp.close()


		
		

	
