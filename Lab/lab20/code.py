import pyparsing as p

identifier = p.Word(p.alphas, p.alphanums)
number1 = p.Word(p.nums) + p.Optional(p.Literal('.') +\
	  p.Optional(p.Word(p.nums)))

number2 = p.Literal('.') + p.Word(p.nums)

number = p.Combine(number1) | p.Combine(number2)

k_print   = p.Keyword('print', caseless=True)
op_plus   = p.Literal('+')
op_minus  = p.Literal('-')
op_mult   = p.Literal('*')
op_div    = p.Literal('/')
#op_mod    = p.Literal('%')
op_assign = p.Literal('=')
op_lparen = p.Literal('(')
op_rparen = p.Literal(')')

addop  = op_plus | op_minus
multop = op_mult | op_div

expr   = p.Forward()
factor = op_lparen + expr + op_rparen | identifier | number

term = factor + p.ZeroOrMore(multop + factor)
expr << (term + p.ZeroOrMore(addop + term))

stmt = k_print + expr | identifier + op_assign + expr
stmt_list = p.OneOrMore(stmt)

#stmt_list.validate()

text = '''
	a = 3
	b = a + 0.5
	c = b * 2

	print c'''

try:
	print stmt_list.parseString(text, parseAll=True)
except p.ParseException as e:
	print e
	

