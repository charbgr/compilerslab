#!/usr/bin/python
# -*- coding: utf_8 -*-

"""
A blank (no function) MyParser class, to be used for developing recursive descent parsers.
Uses plex as scanner module.
"""


import plex


class ParseError(Exception):
	""" A user defined exception class, to describe parse errors. """
	pass	# we don't need to override anything here!


class RunError(Exception):
	""" A user defined exception class, to describe runtime errors. """
	pass	# we don't need to override anything here!


class MyParser:
	""" A class encapsulating all parsing (and executing) functionality
	for a particular grammar. """
	
	def __init__(self):
		""" Parser & exec environment initializations. """
		pass


	def create_scanner(self,fp):
		""" Creates a plex scanner for a particular grammar 
		to operate on file object fp. """

		letter = plex.Range("AZaz")
		digit = plex.Range("09")
		name = letter + plex.Rep(letter|digit)
		telesths = plex.Any("+-=*/()")
		floats = plex.Rep(digit) + plex.Str(".") + plex.Rep1(digit)
		space = plex.Any(" \t\n")
		keyword = plex.Str("print")

		lexicon = plex.Lexicon([
			(floats, "FLOATCONST"),
			(keyword, "COMMAND"),
			(telesths, plex.TEXT),
			(name, "IDENTIFIER"),
			(space, plex.IGNORE)
		])
		
		self.scanner = plex.Scanner(lexicon, fp)	# create and store the scanner object
		self.la, self.val = self.next_token()

	def match(self, token):
		if(self.la == token):
			self.la, self.token = self.next_token()
		else:
			raise ParseError("Found %s instead of %s" % (self.la, token))
	
	def addop(self):
		if self.la == '+':
			self.match('+')			
		elif self.la == '-':
			self.match('-')
		else:
			raise ParseError("Addop: Wrong OP %s\n+, - allowed" % self.la)
	
	def multiop(self):
		if self.la == '*':
			self.match('*')			
		elif self.la == '/':
			self.match('/')
		else:
			raise ParseError("Multiop: Wrong OP%s\n*, / allowed"% self.la)

	def factor(self):
		if self.la == '(':
			self.match('(')
			self.expr()
			self.match(')')
		elif self.la in ('FLOATCONST', 'IDENTIFIER'):
			self.match(self.la)
		else:
			raise ParseError("Factor: %s Wr0nngg\n FLOATCONST, IDENTIFIER all0wed"% self.la)

	def factor_tail(self):
		if self.la in ('*', '/'):
			self.multiop()
			self.factor()
			self.factor_tail()
		elif self.la in ('EOT', '+', '-', 'IDENTIFIER', 'COMMAND',')'):
			return
		else:
			raise ParseError("FACTOR TAIL PROBLEM")

	def stmt(self):
		if self.la == 'IDENTIFIER':
			self.match('IDENTIFIER')
			self.match('=')
			self.expr()
			

		elif self.la == 'COMMAND':
			self.match('COMMAND')
			self.expr()
		else:
			raise ParseError("Stmt: %s Wr0nngg\n FLOATCONST, IDENTIFIER all0wed"% self.la)

	def expr(self):
		if self.la in ('(', 'FLOATCONST', 'IDENTIFIER'):
			self.term()
			self.term_tail()
		else:
			raise ParseError("Expr: %s Wr0nngg\n FLOATCONST, IDENTIFIER all0wed"% self.la)

	def term(self):
		if self.la in ('(', 'FLOATCONST', 'IDENTIFIER'):	
			self.factor()
			self.factor_tail()
		else:
			raise ParseError("Term: %s Wr0nngg\n (, FLOATCONST, IDENTIFIER all0wed"% self.la)
	
	def term_tail(self):
		if self.la in ('+', '-'):
			self.addop()
			self.term()
			self.term_tail()
		elif self.la in ('EOT', '+', '-', 'IDENTIFIER', 'COMMAND',')'):
			return
		else:
			raise ParseError("TERM TAIL PROBLEM")

	def stmt_list(self):
		if self.la in ('IDENTIFIER', 'COMMAND'):
			self.stmt()
			self.stmt_list()
		elif self.la == 'EOT':
			return
		else:
			raise ParseError("STMT LIST")

	def next_token(self):
		""" Returns tuple (next_token,matched-text), 
		    if end-of-text returns ('EOT','').
		    Scanner errors are expected to be handled in outer code. """
		
		token,matched = self.scanner.read()		
		if token==None: return ('EOT','')
		return (token,matched)
	
	
	def parse_file(self,fp):
		""" Creates scanner for input file object fp and calls the parse logic code. """
		
		# create the plex scanner for fp
		self.create_scanner(fp)
		# call parsing logic
		self.stmt_list()
	

	
	def get_position(self):
		""" Utility function that returns position in text in case of errors.
		Here it simply returns the scanner position. """
		
		return self.scanner.position()
	


# the main part of prog

# create the parser object
parser = MyParser()

# open file for parsing
fp = open("some-parsing.txt","r")

# parse file and execute instructions
try:
	parser.parse_file(fp)
except plex.errors.PlexError:
	fname,lineno,charno = parser.get_position()	
	print "Scanner Error: at line %s char %s" % (lineno,charno+1)
except ParseError as perr:
	fname,lineno,charno = parser.get_position()	
	print "Parser Error: %s at line %s" % (perr,lineno)
except RunError as rerr:
	fname,lineno,charno = parser.get_position()	
	print "Runtime Error: %s at line %s" % (rerr,lineno)
finally:
	fp.close()	# close file, no matter what happens in parse_file()


#A -> αΒβ    | FOLLOW(B) <---(+) first(β) //εκτος του εε
#Α -> αΒ     | FOLLOW(B) <---(+) follow(A)
#Α -> αΒβε   | FOLLOW(B) <---(+) FOLLOW(A)
 
