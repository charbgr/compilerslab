#!/usr/bin/env python
# -*- coding: utf-8 -*-

def getchar(words, pos):
	if pos>=len(words): return None
	return words[pos]

def scan(text, transTable, ta):
	"""
		checks text if is date and return DATE_TOKEN if is date else ERROR_TOKEN
		function doesn't check for 30/2

		text -- the date we want
		transTable -- the transition transTable
		ta -- an array of constant acceptable tokens
	"""
	pos = 0
	state = 'q0'
	while True:
		curr_char = getchar(text,pos)
		if curr_char in transTable[state]:
			state = transTable[state][curr_char]
			pos += 1
			if state in ta:
				return ta[state], pos
		else:
			return 'ERROR_TOKEN', pos
	return token, pos


if __name__ == "__main__":
	
	transition_table = {
		'q0':{ '0':'q1', '1':'q4', '2':'q4',
	    	   '3':'q5', '4':'q2', '5':'q2',
	    	   '6':'q2', '7':'q2', '8':'q2',
	    	   '9':'q2' },

		'q1':{ '1':'q2', '2':'q2', '3':'q2',
	    	   '4':'q2', '5':'q2', '6':'q2',
	    	   '7':'q2', '8':'q2', '9':'q2' },

		'q2':{ '/':'q3', '-':'q3' },

		'q3':{ '0':'q8', '1':'q7', '2':'q9',
	    	   '3':'q9', '4':'q9', '5':'q9',
	    	   '6':'q9', '7':'q9', '8':'q9',
	    	   '9':'q9' },

		'q4':{ '0':'q2', '1':'q2', '2':'q2', 
	    	   '3':'q2', '4':'q2', '5':'q2',
	    	   '6':'q2', '7':'q2', '8':'q2',
	    	   '9':'q2', '/':'q3', '-':'q3'},

		'q5':{ '0':'q2', '1':'q2', '/':'q3',
	    	   '-':'q3' },

		'q7':{ '0':'q9', '1':'q9', '2':'q9',
	    	   '/':'q10', '-':'q10' },

		'q8':{ '1':'q9', '2':'q9', '3':'q9',
	    	   '4':'q9', '5':'q9', '6':'q9',
	    	   '7':'q9', '8':'q9', '9':'q9' },

		'q9':{ '/':'q10', '-':'q10' },

		'q10':{ '1':'q11', '2':'q12' },

		'q11':{ '9':'q13' },

		'q12':{ '0':'q13' },

		'q13':{ '0':'q14', '1':'q14', '2':'q14',
	    		'3':'q14', '4':'q14', '5':'q14',
	    		'6':'q14', '7':'q14', '8':'q14',
	    		'9':'q14'},

		'q14':{ '0':'DATE', '1':'DATE', '2':'DATE',
	    	    '3':'DATE', '4':'DATE', '5':'DATE',
	    	    '6':'DATE', '7':'DATE', '8':'DATE',
	    	    '9':'DATE'},
	}
 
	ta = {
	    'DATE': 'DATE_TOKEN'
	}

	import sys
	keywords = []

	if(len(sys.argv) == 1):
		text = raw_input('give some input>')
		keywords.append(text)
	else:
		keywords = sys.argv[1:]

	for text in keywords:
		pos = 0
		while len(text)>0:
			tok,pos = scan(text, transition_table, ta)
			if tok == 'ERROR_TOKEN':
				print 'unrecognized input at pos=',pos,'of \'',text, '\''
				break
			print "token = \'", tok, "\', text = \'", text[:pos], "\', nextpos = \'", pos, "\'"
			text = text[pos:]