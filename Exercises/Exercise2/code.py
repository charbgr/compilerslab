import urllib
import re

def parseHTML(regxlist, url=None):
	if(url is None):
		url = "http://di.ionio.gr/~mistral"

	fp = urllib.urlopen(url)
	html = fp.read()
	fp.close()

	for reg in regxlist:
		html = re.sub(reg, '', html)

	return html


if __name__ == '__main__':
	comments = re.compile(r'<!--.*?-->', re.DOTALL)
	scripts = re.compile(r'<(script|style).+?</(script|style)>', re.DOTALL|re.IGNORECASE)
	basic = re.compile(r'<(?:\"[^\"]*\"[\'\"]*|\'[^\']*\'[\'\"]*|[^\'\">])+>', re.DOTALL)

	#basic must be at the end
	html = parseHTML([comments, scripts, basic])
	fp = open("parsed.txt",'w')
	fp.write(html);
	fp.close()